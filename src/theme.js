const scale = require("./colors.json");
const dark_colors = require("./vars.js");

function get_theme({ style, name })
{
	colors = dark_colors({ style });
	return {
		name: name,
		colors: {
			focusBorder: 			colors.border.emphasis,
			foreground: 			colors.fg.default,
			descriptionForeground: 	colors.fg.muted,
			errorForeground: 		colors.danger.fg,

			"textLink.foreground": 			colors.accent.fg,
			"textLink.activeForeground":	colors.accent.fg,
			"textBlockQuote.background": 	colors.canvas.inset,
			"textBlockQuote.border": 		colors.border.default,
			"textCodeBlock.background": 	colors.neutral.muted,
			"textPreFormat.foreground": 	colors.fg.muted,
			"textSeparator.foreground":		colors.border.muted,

			"icon.foreground": 				colors.fg.muted,
			"keybindingLabel.foreground": 	colors.fg.default,

			"button.background": 			colors.btn.primary.bg,
			"button.foreground":			colors.btn.primary.text,
			"button.hoverBackground": 		colors.btn.primary.hoverBg,

			"button.secondaryBackground": 		colors.btn.activeBg,
			"button.secondaryForeground": 		colors.btn.text,
			"button.secondaryHoverBackground": 	colors.btn.hoverBg,

			"checkbox.background":	colors.canvas.subtle,
			"checkbox.border":		colors.border.default,

			"dropdown.background":		colors.canvas.overlay,
			"dropdown.border":			colors.border.default,
			"dropdown.foreground":		colors.fg.default,
			"dropdown.listBackground":	colors.canvas.overlay,

			"input.background":				colors.canvas.default,
			"input.border":					colors.border.default,
			"input.foreground":				colors.fg.default,
			"input.placeholderForeground":	colors.fg.subtle,

			"badge.foreground":	colors.fg.onEmphasis,
			"badge.background":	colors.accent.emphasis,

			"progressBar.foreground": colors.accent.emphasis,

			"titleBar.activeForeground": 	colors.fg.muted,
			"titleBar.activeBackground": 	colors.canvas.inset,
			"titleBar.inactiveForeground": 	colors.fg.subtle,
			"titleBar.inactiveBackground": 	colors.canvas.subtle,
			"titleBar.border": 				colors.border.default,

			"activityBar.foreground":         colors.fg.default,
			"activitBar.inactiveForeground":  colors.fg.muted,
			"activityBar.background":         colors.canvas.default,
			"activityBarBadge.foreground":    colors.fg.onEmphasis,
			"activityBarBadge.background":    colors.accent.emphasis,
			"activityBar.activeBackground":   colors.canvas.inset,
			"activityBar.border":             colors.border.default,

			"sideBar.foreground": 				colors.fg.default,
			"sideBar.background": 				colors.canvas.inset,
			"sideBar.border": 					colors.border.default,
			"sideBarTitle.foreground": 			colors.fg.subtle,
			"sideBarSectionHeader.foreground": 	colors.fg.default,
			"sideBarSectionHeader.background": 	colors.canvas.subtle,
			"sideBarSectionHeader.border":		colors.canvas.subtle,

			"list.hoverForeground":					colors.fg.onEmphasis,
			"list.inactiveSelectionForeground": 	colors.fg.subtle,
			"list.activeSelectionForeground": 		colors.fg.onEmphasis,
			"list.hoverBackground":					colors.canvas.overlay,
			"list.inactiveSelectionBackground":		colors.canvas.subtle,
			"list.activeSelectionBackground": 		colors.canvas.overlay,
			"list.focusForeground":					colors.fg.onEmphasis,
			"list.focusBackground":					colors.canvas.subtle,
			"list.inactiveFocusBackground":			colors.canvas.subtle,
			"list.highlightForeground":				colors.accent.fg,
			"list.dropBackground":					colors.canvas.subtle,

			"tree.indentGuidesStroke": colors.border.emphasis,

			"notificationCenterHeader.foreground": colors.fg.default,
			"notificationCenterHeader.background": colors.canvas.inset,
			"notifications.foreground": colors.fg.default,
			"notifications.background": colors.canvas.inset,
			"notification.border": colors.border.default,
			"notificationsErrorIcon.foreground": colors.danger.fg,
			"notificationsWarningIcon.foreground": colors.attention.fg,
			"notificationsInfoIcon.foreground": colors.accent.fg,

			"pickerGroup.foreground":	colors.fg.subtle,
			"pickerGroup.border":		colors.border.default,
			"quickInput.foreground":	colors.fg.default,
			"quickInput.background":	colors.canvas.subtle,

			"statusBar.foreground": 				colors.fg.subtle,
			"statusBar.background": 				colors.canvas.inset,
			"statusBar.border": 					colors.border.default,
			"statusBar.focusBorder":				colors.border.default,
			"statusBar.noFolderForeground":			colors.fg.subtle,
			"statusBar.noFolderBackground": 		colors.danger.bg,
			"statusBar.noFolderBorder": 			colors.danger.bg,
			"statusBar.debuggingForeground":		colors.fg.subtle,
			"statusBar.debuggingBackground":		colors.attention.bg,
			"statusBar.debuggingBorder":			colors.attention.bg,
			"statusBarItem.prominentBackground":	colors.neutral.muted,
			"statusBarItem.remoteForeground":		colors.fg.default,
			"statusBarItem.remoteBackgroud":		colors.match.bg,
			"statusBarItem.hoverBackground":		colors.canvas.default,
			"statusBarItem.activeBackground":		colors.canvas.overlay,

			"editorGroupHeader.tabsBackground":		colors.canvas.inset,
			"editorGroupHeader.tabsBorder":			colors.canvas.inset,
			"editorGroupHeader.border":				colors.canvas.inset,

			"tab.activeForeground":			colors.fg.default,
			"tab.activeBackground":			colors.canvas.default,
			"tab.inactiveForeground":		colors.fg.subtle,
			"tab.inactiveBackground":		colors.canvas.inset,
			"tab.hoverForeground":			colors.fg.default,
			"tab.hoverBackground":			colors.canvas.overlay,
			"tab.unfocusedHoverBackground": colors.canvas.overlay,
			"tab.border":					colors.border.muted,
			"tab.borderTop":				colors.border.default,
			"tab.activeBorder":				colors.canvas.black,
			"tab.activeBorderTop":			colors.canvas.black,
			"tab.unfocusedActiveBorderTop": colors.canvas.inset,
			"tab.unfocusedActiveBorder":	colors.canvas.inset,

			"breadcrumb.background": 		colors.canvas.default,
			"breadcrumb.foreground":		colors.fg.default,
			"breadcrumb.focusForeground":	colors.fg.onEmphasis,
			"breadcrumbPicker.background":	colors.canvas.subtle,

			"editor.background": 					colors.canvas.default,
			"editor.foreground":					colors.fg.onEmphasis,
			"editorWidget.foreground":				colors.fg.subtle,
			"editorWidget.background":				colors.canvas.inset,
			"editorWidget.resizeBorder":			colors.border.muted,
			"editor.foldBackground":				colors.neutral.emphasis,
			"editor.lineHighlightBackground":		"#00000000",
			"editor.lineHighlightBorder":			"#00000000",
			"editorLineNumber.foreground": 			colors.canvas.overlay,
			"editorLineNumber.activeForeground":	colors.fg.muted,
			"editorIndentGuide.background": 		colors.border.muted,
			"editorIndentGuide.activeBackground":	colors.border.emphasis,
			"editorWhitespace.foreground":			colors.canvas.subtle,
			"editorCursor.foreground":				colors.fg.cursor,
			"editorCursor.background":				colors.canvas.default,

			"editor.findMatchBackground":			colors.match.bg,
			"editor.findMatchHighlightBackground":	colors.match.fg,
			"editor.linkedEditingBackground":		scale.magenta[4], // #todo
			"editor.selectionBackground":			colors.canvas.subtle,
			"editor.inactiveSelectionBackground":	colors.canvas.inset,
			"editor.selectionHighlightBackground":	colors.canvas.overlay,
			"editor.selectionHighlightBorder":		colors.canvas.overlay,
			"editor.wordHighlightBackground":		colors.accent.bg + "00",
			"editor.wordHighlightBorder":			colors.accent.bg + "00",
			"editor.wordHighlightStrongBackground":	colors.danger.bg + "00",
			"editor.wordHighlightStrongBorder":		colors.danger.bg + "00",
			"editorBracketMatch.background":		colors.match.bg,
			"editorBracketMatch.border":			colors.match.bg,

			// #todo untested
			"editorInLayHint.foreground":		colors.fg.muted,
			"editorInLayHint.background":		colors.canvas.inset,
			"editorInLayHint.typeForeground":	colors.accent.emphasis,
			"editorInLayHint.typeBackground":	colors.canvas.inset,
			"editorInLayHint.paramForeground":	colors.fg.muted,
			"editorInLayHint.paramBackground":	colors.canvas.inset,

			"editorGutter.modifiedBackground":	colors.attention.fg,
			"editorGutter.deletedBackground":	colors.danger.emphasis,
			"editorGutter.insertedBackground":	colors.success.emphasis,
			"editorGutter.background": 			colors.canvas.default,
			"editorGutter.foldBackground":		colors.canvas.inset,

			"diffEditor.insertedTextBackground":	colors.success.fg + "40",
			"diffEditor.insertedTextBorder": 		colors.success.fg + "00",
			"diffEditor.removedTextBackground":		colors.danger.fg + "40",
			"diffEditor.removedTextBorder":			colors.danger.fg + "00",
			"diffEditor.border":					colors.border.muted,

			"minimapSlider.background":			colors.canvas.inset + "a0",
			"minimapSlider.hoverBackground":	colors.canvas.subtle + "a0",
			"minimapSlider.activeBackground":	colors.canvas.subtle + "a0",

			"panel.background":					colors.canvas.default,
			"panel.border":						colors.canvas.inset,
			"panelTitle.activeBorder":			colors.fg.default,
			"panelTitle.inactiveBorder":		colors.fg.muted,
			"panelTitle.inactiveForeground":	colors.fg.muted,
			"panelInput.border":				colors.border.default,

			"debugIcon.breakpointForeground":	colors.danger.fg,

			"debugConsole.infoForeground":		colors.accent.fg,
			"debugConsole.warningForeground":	colors.attention.fg,
			"debugConsole.errorForeground":		colors.danger.fg,
			"debugConsole.sourceForeground":	colors.severe.fg,
			"debugConsoleInputIcon.foreground":	colors.success.fg, // #todo

			"debugTokenExpression.name":	colors.accent.fg,
			"debugTokenExpression.value":	colors.fg.default,
			"debugTokenExpression.string":	colors.syntax.string,
			"debugTokenExpression.boolean":	colors.syntax.type,
			"debugTokenExpression.number":	colors.syntax.numeral,
			"debugTokenExpression.error":	colors.danger.fg,

			"symbolIcon.functionForeground":			colors.syntax.function,
			"symbolIcon.methodForeground":				colors.syntax.function,
			"symbolIcon.arrayForeground": 				colors.syntax.type,
			"symbolIcon.booleanForeground":				colors.syntax.type,
			"symbolIcon.classForeground":				colors.syntax.type,
			"symbolIcon.constructorForeground":			colors.syntax.type,
			"symbolIcon.enumeratorForeground":			colors.syntax.type,
			"symbolIcon.interfaceForeground":			colors.syntax.type,
			"symbolIcon.structForeground":				colors.syntax.type,
			"symbolIcon.typeParameterForeground":		colors.syntax.type,
			"symbolIcon.eventForeground":				colors.syntax.type,
			"symbolIcon.objectForeground":				colors.syntax.type,
			"symbolIcon.keyForeground":					colors.syntax.type,
			"symbolIcon.moduleForeground":				colors.syntax.namespace,
			"symbolIcon.namespaceForeground":			colors.syntax.namespace,
			"symbolIcon.packageForeground":				colors.syntax.namespace,
			"symbolIcon.fieldForeground":				colors.syntax.field,
			"symbolIcon.referenceForeground":			colors.syntax.field,
			"symbolIcon.propertyForeground":			colors.syntax.property,
			"symbolIcon.enumeratorMemberForeground":	colors.syntax.enum,
			"symbolIcon.keywordForeground":				colors.syntax.keyword,
			"symbolIcon.nullForeground":				colors.syntax.keyword,
			"symbolIcon.numberForeground":				colors.syntax.numeral,
			"symbolIcon.colorForeground":				colors.syntax.numeral,
			"symbolIcon.constantForeground":			colors.syntax.numeral,
			"symbolIcon.stringForeground":				colors.syntax.string,
			"symbolIcon.textForeground":				colors.fg.subtle,
			"symbolIcon.operatorForeground":			colors.syntax.operator,
			"symbolIcon.snippetForeground":				colors.syntax.operator,
			"symbolIcon.variableForeground":			colors.syntax.variable,
			"symbolIcon.fileForeground":				colors.success.fg,
			"symbolIcon.folderForeground":				colors.success.fg,
			"symbolIcon.unitForeground":				colors.success.fg,

			"terminal.selectionBackground": colors.canvas.overlay,
			"terminal.border":				colors.border.default,
			"terminalCursor.foreground": 	colors.fg.cursor,
			"terminalCurser.background": 	colors.canvas.default,

			"terminal.foreground":			colors.fg.default,
			"terminal.ansiWhite":			colors.ansi.white,
			"terminal.ansiBlack":			colors.ansi.black,
			"terminal.ansiBlue":			colors.ansi.blue,
			"termianl.ansiCyan":			colors.ansi.cyan,
			"terminal.ansiGreen":			colors.ansi.green,
			"terminal.ansiMagenta":			colors.ansi.magenta,
			"terminal.ansiRed":				colors.ansi.red,
			"terminal.ansiYellow":			colors.ansi.yellow,
			"terminal.ansiBrightWhite":		colors.ansi.whiteBright,
			"terminal.ansiBrightBlack":		colors.ansi.blackBright,
			"terminal.ansiBrightBlue":		colors.ansi.blueBright,
			"terminal.ansiBrightCyan":		colors.ansi.cyanBright,
			"terminal.ansiBrightGreen":		colors.ansi.greenBright,
			"terminal.ansiBrightMagenta":	colors.ansi.magentaBright,
			"terminal.ansiBrightRed":		colors.ansi.redBright,
			"terminal.ansiBrightYellow":	colors.ansi.yellowBright,

			"gitDecoration.untrackedResourceForeground":		colors.git.untracked,
			"gitDecoration.modifiedResourceForeground":			colors.git.modified,
			"gitDecoration.deletedResourceForeground":			colors.git.deleted,
			"gitDecoration.addedResourceForeground":			colors.git.added,
			"gitDecoration.stageModifiedResourceForeground":	colors.git.stageModified,
			"gitDecoration.stageDeletedResourceForeground":		colors.git.stageDeleted,
			"gitDecoration.conflictingResourceForeground":		colors.git.conflict,
			"gitDecoration.submoduleResourceForeground":		colors.git.submodule,
			"gitDecoration.renamedResourceForeground":			colors.git.renamed,
			"gitDecoration.ignoredResoureForeground":			colors.git.ignored,

			"debugToolbar.background":	colors.canvas.overlay,
			"debugToolbar.border":		colors.canvas.overlay,

			"peekView.border":							colors.border.muted,
			"peekViewEditor.background":				colors.canvas.inset,
			"peekViewEditor.matchHighlightBackground":	colors.match.bg,
			"peekViewEditor.matchHighlightBorder":		colors.match.bg,
			"peekViewEditorGutter.background":			colors.canvas.inset,
			"peekViewResult.background":				colors.canvas.default,
			"peekViewResult.fileForeground":			colors.fg.subtle,
			"peekViewResult.lineForeground":			colors.fg.subtle,
			"peekViewResult.matchHighlightBackground":	colors.match.bg,
			"peekViewResult.matchHighlightBorder":		colors.match.bg,
			"peekViewResult.selectionBackground":		colors.canvas.overlay,
			"peekViewResult.selectionForeground":		colors.fg.onEmphasis,
			"peekViewTitle.background":					colors.canvas.default,
			"peekViewTitleLabel.foreground":			colors.fg.default,
			"peekViewTitleLabel.background":			colors.canvas.inset,
			"peekViewTitle.background":					colors.canvas.default,
			"peekViewTitleDescription.foreground":		colors.fg.subtle,

			"settings.headerForeground":			colors.fg.default,
			"settings.modifiedItemIndicator":		colors.attention.muted,
			"welcomePage.buttonBackground":			colors.btn.bg,
			"welcomePage.buttonHoverBackground":	colors.btn.hoverBg,

			"inputOption.activeBackground":	colors.canvas.subtle,
			"inputOption.activeBorder":		colors.border.muted,
			"inputOption.activeForeground": colors.fg.default,

			"selection.background":				colors.canvas.overlay,
			"scrollbar.shadow":					colors.shadow.subtle,
			"scrollbarSlider.background":		colors.canvas.subtle + "80",
			"scrollbarSlider.hoverBackground":	colors.canvas.overlay + "80",
			"scrollbarSlider.activeBackground":	colors.canvas.subtle + "80",

			"listFilterWidget.background":			colors.canvas.default,
			"listFilterWidget.outline":				colors.shadow.subtle,
			"listFilterWidget.noMatchesOutline":	colors.danger.bg,

			"menubar.selectionForeground":	colors.fg.default,
			"menubar.selectionBackground":	colors.canvas.subtle,
			"menubar.selectionBorder":		colors.canvas.subtle,

			"menu.foreground":				colors.fg.subtle,
			"menu.background":				colors.canvas.inset,
			"menu.selectionForeground":		colors.fg.default,
			"menu.selectionBackground":		colors.canvas.subtle,
			"menu.selectionBorder":			colors.border.muted,
			"menu.separatorBackground":		colors.border.emphasis,
			"menu.border":					colors.shadow.subtle,

			"widget.shadow": colors.shadow.subtle,

			"notificationToast.border":		colors.border.muted,
			"notificationCenter.border":	colors.border.muted,

			"editor.matchHighlightBackground":		colors.match.emphasis,
			"editor.findMatchBorder":				colors.match.fg,
			"editor.findMatchHighlightBorder":		colors.match.bg,
			"editor.findRangeHighlightBackground":	colors.match.emphasis,
			"editor.findRangeHighlightBorder":		colors.match.emphasis,
			"editor.rangeHighlightBackground":		colors.match.bg,
			"editor.rangeHighlightBorder":			colors.match.bg,
			"editor.hoverHighlightBackground":		colors.match.fg,

			"editorLink.activeForeground":	colors.accent.fg,

			"editorRuler.foreground": colors.canvas.inset,

			"editorOverviewRuler.border": 						colors.border.default,
			"editorOverviewRuler.background": 					colors.canvas.inset,
			"editorOverviewRuler.foreground":					colors.fg.muted,
			"editorOverviewRuler.findMatchForeground":			colors.match.fg,
			"editorOverviewRuler.currentFindMatchForeground":	colors.match.bg,
			"editorOverviewRuler.nextFindMatchForeground":		colors.match.bg,
			"editorOverviewRuler.errorForeground":				colors.danger.fg,
			"editorOverviewRuler.warningForeground":			colors.attention.fg,
			"editorOverviewRuler.infoForeground":				colors.accent.fg,
			"editorOverviewRuler.modifiedForeground":			colors.git.modified,
			"editorOverviewRuler.addedForeground":				colors.git.added,
			"editorOverviewRuler.deletedForeground":			colors.git.deleted,

			"editorError.foreground":	"#00000000", //colors.danger.fg,
			"editorError.border":		colors.danger.fg,
			"editorError.background":	"#00000000",
			"editorWarning.foreground":	"#00000000", //colors.attention.fg,
			"editorWarning.border":		colors.attention.fg,
			"editorWarning.background": "#00000000",
			"editerInfo.foreground":	"#00000000", //colors.accent.fg,
			"editorInfo.border":		colors.accent.fg,
			"editorInfo.background":	"#00000000",

			"editorCodeLens.foreground":	colors.neutral.muted,

			"editorGroup.border":			colors.border.muted,

			"editorMarkerNavigation.background":		colors.canvas.inset,
			"editorMarkerNavigation.foreground":		colors.neutral.muted,
			"editorMarkerNavigationError.foreground":	colors.danger.fg,
			"editorMarkerNavigationError.background":	colors.danger.bg,
			"editorMarkerNavigationWarning.foreground": colors.attention.fg,
			"editorMarkerNavigationWarning.background": colors.attention.bg,
			"editorMarkerNavigationInfo.foreground":	colors.accent.fg,
			"editorMarkerNavigationInfo.background":	colors.accent.bg,

			"merge.currentHeaderBackground":	colors.git.mergeCurrent.header,
			"merge.currentContentBackground":	colors.git.mergeCurrent.content,
			"merge.incomingHeaderBackground":	colors.git.mergeIncoming.header,
			"merge.incomingContentBackground":	colors.git.mergeIncoming.content,
			"merge.commonHeaderBackground":		colors.canvas.overlay,
			"merge.commonContentBackground":	colors.canvas.inset,

			"editorSuggestWidget.background":			colors.canvas.inset,
			"editorSuggestWidget.border":				colors.border.default,
			"editorSuggestWidget.foreground":			colors.fg.muted,
			"editorSuggestWidget.selectedBackground":	colors.canvas.subtle,
			"editorSuggestWidget.highlightForeground":	colors.accent.fg,
			"editorSuggestWidget.selectedForeground":	colors.fg.subtle,

			"editorHoverWidget.background":	colors.canvas.inset,
			"editorHoverWidget.border":		colors.border.default,
			"editorHoverWidget.foreground":	colors.fg.muted,
		},
		semanticHighlighting: true,
		semanticTokenColors: {
			"property":		colors.syntax.property,
			"field":		colors.syntax.field,
			"parameter":	colors.syntax.parameter,
			"enumMember":	colors.syntax.enum,
			"variable":		colors.syntax.variable,

			"xmlDocCommentDelimiter":		colors.syntax.docComment.start,
			"xmlDocCommentText":			colors.syntax.docComment.content,
			"xmlDocCommentName":			colors.syntax.keyword,
			"xmlDocCommentAttributeName":	colors.syntax.parameter,
		},
		tokenColors: [
			{
				scope: [
					"punctuation.definition.comment"
				],
				settings: {
					foreground: colors.syntax.comment.start,
				}
			},
			{
				scope: [
					"comment",
					"comment.block",
					"comment.line"
				],
				settings: {
					foreground: colors.syntax.comment.content,
				}
			},
			{
				scope: [
					"punctuation.definition.comment.begin.documentation",
					"punctuation.definition.comment.end.documentation",
					"punctuation.definition.comment.documentation"
				],
				settings: {
					foreground: colors.syntax.docComment.start,
				}
			},
			{
				scope: [
					"comment.block.documentation",
					"comment.line.double-slash.documentation",
				],
				settings: {
					foreground: colors.syntax.docComment.content,
				}
			},

			{
				scope: [
					"string",
					"string.regexp"
				],
				settings: {
					foreground: colors.syntax.string,
				}
			},
			{
				scope: [
					"string.quoted.single.c"
				],
				settings: {
					foreground: colors.syntax.char,
					fontStyle: "bold"
				}
			},
			{
				scope: [
					"constant.numeric.decimal"
				],
				settings: {
					foreground: colors.syntax.numeral,
					fontStyle: "bold"
				}
			},
			{
				scope: [
					"keyword.other.unit.hexadecimal",
					"keyword.other.unit.suffix"
				],
				settings: {
					foreground: colors.syntax.operator,
				}
			},
			{
				scope: [
					"meta.object",
					"string.unquoted.plain.out.yaml",
					"variable.other"
				],
				settings: {
					foreground: colors.syntax.variable,
				}
			},
			{
				scope: [
					"keyword.control",
					"variable.language.makefile"
				],
				settings: {
					foreground: colors.syntax.control,
					fontStyle: "bold"
				}
			},
			{
				scope: [
					"keyword",
					"keyword.operator.new",
				],
				settings:
				{
					foreground: colors.syntax.keyword,
				}
			},
			{
				scope: [
					"storage.type",
					"storage.modifier",
					"meta.scope.target.makefile",
					"entity.name.tag.yaml"
				],
				settings: {
					foreground: colors.syntax.keyword,
					fontStyle: "bold"
				}
			},
			{
				scope: [
					"entity.name.type",
					"entity.name.function.target.makefile"
				],
				settings: {
					foreground: colors.syntax.type,
				}
			},
			{
				scope: [
					"entity.name.type.namespace"
				],
				settings: {
					foreground: colors.syntax.namespace,
				}
			},
			{
				scope: [
					"storage.modifier"
				],
				settings: {
					foreground: colors.syntax.keyword,
				}
			},
			{
				scope: [
					"entity.name.function",
					"support.function"
				],
				settings: {
					foreground: colors.syntax.function,
					fontStyle: "bold"
				}
			},
			{
				scope: [
					"punctuation.definition.directive"
				],
				settings: {
					foreground: colors.syntax.preprocess.start,
				}
			},
			{
				scope: [
					"keyword.control.directive"
				],
				settings: {
					foreground: colors.syntax.preprocess.content,
				}
			},
			{
				scope: [
					"entity.name.function.preprocessor"
				],
				settings: {
					foreground: colors.syntax.macro,
				}
			},
			{
				scope: [
					"string.quoted.other.lt-gt.include",
					"string.quoted.double.include"
				],
				settings: {
					foreground: colors.syntax.preprocess.string,
				}
			},
			{
				scope: [
					"operator",
					"keyword.operator",
					"punctuation.separator",
					"punctuation.terminator",
					"punctuation.section",
					"punctuation.accessor",
					"meta.brace",
					"punctuation.definition.parameters",
				],
				settings: {
					foreground: colors.syntax.operator,
				}
			},
			{
				scope: [
					"punctuation.definition.begin.bracket",
					"punctuation.definition.end.bracket"
				],
				settings: {
					foreground: colors.syntax.operator,
				}
			},
			{
				scope: [
					"entity.name.variable.field",
					"punctuation.definition.variable",
					"variable.other.makefile",
					"variable.other.member"
				],
				settings: {
					foreground: colors.syntax.field,
				}
			},
			{
				scope: [
					"variable.other.object.property"
				],
				settings: {
					foreground: colors.syntax.property,
				}
			},
			{
				scope: [
					"punctuation.definition.heading.markdown",
				],
				settings: {
					foreground: colors.syntax.keyword,
					fontStyle: "bold",
				}
			},
			{
				scope: [
					"entity.name.section.markdown",
					"markup.heading.markdown",
				],
				settings: {
					foreground: colors.syntax.type,
					fontStyle: "bold",
				}
			},
		]
	};
}

module.exports = get_theme;
