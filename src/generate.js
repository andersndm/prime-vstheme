const fs = require("fs");
const get_theme = require("./theme");

function write_theme(returnValue, filePath) {
  // Convert the return value to a string
  const data = JSON.stringify(returnValue, null, 4);

  // Write the data to the file
  fs.writeFile(filePath, data, 'utf8', (err) => {
    if (err) {
      console.error('Error writing to file:', err);
      return;
    }
    console.log(filePath + ' written successfully')
  });
}

const dark_name = "prime-dark";
const dark_theme = get_theme({ style: "dark", name: dark_name});
const dark_path = "../themes/" + dark_name + ".json";

write_theme(dark_theme, dark_path);

const dim_name = "prime-dim";
const dim_theme = get_theme({ style: "dim", name: dim_name});
const dim_path = "../themes/" + dim_name + ".json";
write_theme(dim_theme, dim_path);

const light_name = "prime-light";
const light_theme = get_theme({ style: "light", name: light_name});
const light_path = "../themes/" + light_name + ".json";
write_theme(light_theme, light_path);