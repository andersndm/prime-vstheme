const scale = require("./colors.json");

function dark_vars()
{
	return {
		fg: {
			default: 	scale.grey[3],
			muted: 		scale.grey[5],
			subtle:		scale.grey[4],
			onEmphasis:	scale.grey[2],
			cursor:		scale.grey[1],
		},
		canvas: {
			default:	scale.black,
			overlay:	scale.grey[7],
			inset:		scale.grey[9],
			subtle:		scale.grey[8],
		},
		border: {
			default:	scale.grey[9],
			muted:		scale.grey[8],
			emphasis:	scale.grey[6],
		},
		shadow: {
			subtle: 	"#00000022",
		},

		match: {
			emphasis:	scale.cyan[3],
			fg:			scale.cyan[4],
			bg: 		scale.cyan[6],
		},
		neutral: {
			muted:		scale.grey[6],
			emphasis:	scale.grey[9] + "80",
		},
		accent: {
			emphasis: 	scale.blue[6],
			fg: 		scale.blue[4],
			bg: 		scale.blue[8],
		},
		success: {
			emphasis:	scale.green[3],
			fg:			scale.green[4],
			bg:			scale.green[8],
		},
		attention: {
			emphasis:	scale.purple[3],
			fg: 		scale.purple[4],
			bg:			scale.purple[8],
		},
		severe: {
			emphasis:	scale.yellow[3],
			fg: 		scale.yellow[4],
			bg:			scale.yellow[8],
		},
		danger: {
			emphasis:	scale.red[3],
			fg: 		scale.red[4],
			bg: 		scale.red[8],
		},

		syntax: {
			type:			scale.blue[5],
			namespace:		scale.blue[4],
			function:		scale.purple[5],
			property:		scale.grey[2],
			field:			scale.grey[2],
			enum:			scale.blue[3],
			keyword:		scale.blue[6],
			numeral:		scale.white,
			string:			scale.yellow[2],
			char:			scale.yellow[3],
			operator:		scale.grey[3],
			variable:		scale.grey[2],
			parameter:		scale.grey[3],
			control:		scale.blue[4],
			macro:			scale.red[4],

			comment: {
				start:		scale.grey[7],
				content:	scale.grey[5],
			},

			docComment: {
				start:		scale.green[7],
				content:	scale.green[5],
			},

			preprocess: {
				start:		scale.red[7],
				content:	scale.red[6],
				string:		scale.purple[3],
			},
		},

		ansi: {
			white:		scale.grey[2],
			black:		scale.grey[6],
			blue:		scale.blue[4],
			cyan:		scale.cyan[4],
			green:		scale.green[4],
			magenta:	scale.purple[4],
			red:		scale.red[4],
			yellow:		scale.yellow[4],

			whiteBright:	scale.grey[1],
			blackBright:	scale.grey[5],
			blueBright:		scale.blue[3],
			cyanBright:		scale.cyan[3],
			greenBright:	scale.green[3],
			magentaBright:	scale.purple[3],
			redBright:		scale.red[3],
			yellowBright:	scale.yellow[3],
		},

		git: {
			untracked:		scale.green[3],
			modified:		scale.purple[3],
			deleted:		scale.red[3],
			added:			scale.green[4],
			stagedModified:	scale.purple[4],
			stagedDeleted:	scale.red[4],
			confilct:		scale.red[6],
			submodule:		scale.cyan[3],
			ignored:		scale.grey[6],
			renamed:		scale.cyan[4],

			mergeCurrent: {
				header:		scale.cyan[5],
				content:	scale.cyan[7],
			},
			mergeIncoming: {
				header:		scale.blue[5],
				content:	scale.blue[7],
			},
		},

		btn: {
			activeBg:	scale.grey[7],
			text: 		scale.grey[2],
			hoverBg:	scale.grey[6],
			primary: {
				bg:			scale.blue[6],
				text:		scale.grey[2],
				hoverBg:	scale.blue[5],
			}
		}
	}
}

function dim_vars()
{
	result = dark_vars();
	
	result.fg.default		= scale.grey[2];
	result.fg.muted			= scale.grey[4];
	result.fg.subtle		= scale.grey[3];
	result.fg.onEmphasis	= scale.grey[1];

	result.canvas.default	= scale.grey[8];
	result.canvas.overlay	= scale.grey[5];
	result.canvas.inset		= scale.grey[7];
	result.canvas.subtle	= scale.grey[6];

	return result;
}

function light_vars()
{
	result = dark_vars();

	result.fg.default		= scale.grey[8];
	result.fg.muted			= scale.grey[7];
	result.fg.subtle		= scale.grey[6];
	result.fg.onEmphasis	= scale.grey[0];
	result.fg.cursor		= scale.grey[9];
	
	result.canvas.default	= scale.grey[0]
	result.canvas.overlay	= scale.grey[3];
	result.canvas.inset		= scale.grey[1];
	result.canvas.subtle	= scale.grey[2];

	result.border.default	= scale.grey[1];
	result.border.muted		= scale.grey[2];
	result.border.emphasis	= scale.grey[4];
	
	result.match.emphasis	= scale.cyan[2];
	result.match.fg			= scale.cyan[3];
	result.match.bg			= scale.cyan[2];
	
	result.neutral.muted	= scale.grey[4];
	result.neutral.emphasis= scale.grey[1] + "80";
	
	result.accent.emphasis	= scale.blue[5];
	result.accent.fg		= scale.blue[3];
	result.accent.bg		= scale.blue[4];

	result.success.emphasis	= scale.green[3];
	result.success.fg		= scale.green[2];
	result.success.bg		= scale.green[4];

	result.attention.emphasis	= scale.purple[3];
	result.attention.fg			= scale.purple[2];
	result.attention.bg			= scale.purple[4];

	result.severe.emphasis	= scale.yellow[3];
	result.severe.fg		= scale.yellow[2];
	result.severe.bg		= scale.yellow[4];

	result.danger.emphasis	= scale.red[3];
	result.danger.fg		= scale.red[2];
	result.danger.bg		= scale.red[4];
	
	result.syntax.type		= scale.blue[6];
	result.syntax.namespace = scale.blue[5];
	result.syntax.property	= scale.grey[7];
	result.syntax.field		= scale.grey[7];
	result.syntax.enum		= scale.blue[4];
	result.syntax.numeral	= scale.black;
	result.syntax.string	= scale.yellow[4];
	result.syntax.char		= scale.yellow[5];
	result.syntax.operator	= scale.grey[6];
	result.syntax.variable	= scale.grey[7];
	result.syntax.parameter = scale.grey[8];
	result.syntax.macro		= scale.red[5];
	
	result.syntax.comment.start		= scale.grey[4];
	result.syntax.comment.content	= scale.grey[3];
	
	result.syntax.docComment.start		= scale.green[4];
	result.syntax.docComment.content	= scale.green[3];
	
	result.syntax.preprocess.string	= scale.purple[5];

	return result;
}

function get_colors({style})
{
	switch (style)
	{
		case "dark":
			return dark_vars();
		case "dim":
			return dim_vars();
		case "light":
			return light_vars();
		default:
			throw new Error("invalid style parameter: " + style);
	}
}

module.exports = get_colors;